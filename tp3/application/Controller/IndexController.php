<?php

/**
 * Class IndexController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Libs\Helper;
use Mini\Model\Model;

class IndexController extends AbstractController
{
    public function index() {
    	$this->app->authentificator->checkLogin();

    	$m = new Model();
    	$m->incrementCount();

    	$userID = $this->app->authentificator->getCurrentUserId();

		return [
			'count' => $m->getCount(),
			'user' => $m->getUser($userID),
			'userId' => $userID
		];
    }

	public function login() {
		$this->app->authentificator->checkLoginOnLoginPages();
		$m = new Model();

		if(!empty($_POST)) {
			if(!empty($_POST['id']) && !empty($_POST['pass'])) {
				$logged = $this->app->authentificator->login($_POST['id'], $_POST['pass']);
				if(!$logged) {
					$err = "Authentification impossible";
				} else {
					Helper::redirect(URL);
				}
			}
			else {
				$err = "Des champs requis sont vides";
			}
		}

		return [
			'error' => $err ?? false
		];
	}

	public function register() {
		$this->app->authentificator->checkLoginOnLoginPages();
		$m = new Model();

		if(!empty($_POST)) {
			if(!empty($_POST['id']) &&
			   !empty($_POST['prenom']) &&
			   !empty($_POST['nom']) &&
			   !empty($_POST['pass1']) &&
			   !empty($_POST['pass2']))
			{
				$_POST = array_map('trim', $_POST);

				if($m->getUser($_POST['id'])!==false) {
					$err = "Cet identifiant est déjà pris";
				}
				else if($_POST['pass1'] != $_POST['pass2']){
					$err = "Les mots de passe sont différents";
				}
				else {
					try{
						$m->addUser($_POST['id'], $_POST['nom'], $_POST['prenom'], $_POST['pass1']);
						Helper::redirect(URL.'index/login');
					} catch(\PDOException $e) {
						if($e->errorInfo == 1062) {
							$err = "Cet identifiant est déjà pris";
						}
						else {
							$err = "Erreur lors de l'insertion en base (err" . $e->errorInfo . ')';
						}
					}

				}
			}
			else {
				$err = "Des champs requis sont vides";
			}
		}

		return [
			'error' => $err ?? false
		];
	}

	public function logout(){
    	$this->app->authentificator->logout();
    	Helper::redirect(URL.'index/login');
	}
}
