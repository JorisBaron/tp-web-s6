<?php

namespace Mini\Model;

use PDO;
use PDOException;


class Model
{

	/**
	 * @var PDO Database Connection
	 */
	public static $sDb = null;
	/**
	 * @var PDO Database Connection
	 */
	public $db = null;

	/**
	 * Model constructor.
	 * Design pattern Singleton
	 */
	function __construct() {
		if(!isset(self::$sDb)) {
			try {
				self::openDatabaseConnection();
			}
			catch(PDOException $e) {
				exit('Database connection could not be established.');
			}
		}

		$this->db = self::$sDb;
	}

	/**
	 * Open the database connection with the credentials from application/config/config.php
	 */
	private function openDatabaseConnection() {
		// set the (optional) options of the PDO connection. in this case, we set the fetch mode to
		// "objects", which means all results will be objects, like this: $result->user_name !
		// For example, fetch mode FETCH_ASSOC would return results like this: $result["user_name"] !
		// @see http://www.php.net/manual/en/pdostatement.fetch.php
		$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

		// setting the encoding is different when using PostgreSQL
		if(DB_TYPE=="pgsql") {
			$databaseEncodingenc = " options='--client_encoding=".DB_CHARSET."'";
		}
		else {
			$databaseEncodingenc = "; charset=".DB_CHARSET;
		}

		// generate a database connection, using the PDO connector
		// @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
		self::$sDb = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME.$databaseEncodingenc, DB_USER, DB_PASS, $options);
	}


	/**
	 * @param string $id
	 * @param string $nom
	 * @param string $prenom
	 * @param string $pass
	 * @return bool
	 * @throws PDOException
	 */
	public function addUser(string $id, string $nom, string $prenom, string $pass){
		$req = $this->db->prepare("INSERT INTO `users`(`id`, `nom`, `prenom`, `pass`) VALUES (:id, :n, :p, :pass);");
		$req->bindValue(':id', $id);
		$req->bindValue(':p', $prenom);
		$req->bindValue(':n', $nom);
		$req->bindValue(':pass', password_hash($pass, PASSWORD_DEFAULT));
		$req->execute();
		return $req->rowCount()==1;
	}

	/**
	 * @param string $id
	 * @return bool|object
	 */
	public function getUser(string $id)  {
		$req = $this->db->prepare("SELECT * FROM `users` WHERE `id` = :id;");
		$req->bindValue(':id', $id);
		$req->execute();
		return $req->fetch();
	}

	public function verifyPassword(string $id, string $pass)  {
		$user = $this->getUser($id);
		return $user!==false && password_verify($pass, $user->pass);
	}


	public function getCount() : int{
		$req = $this->db->prepare("SELECT nb FROM `visits`;");
		$req->execute();
		return $req->fetch()->nb;
	}

	public function incrementCount() : void {
		$req = $this->db->prepare("UPDATE `visits` SET `nb` = `nb`+1;");
		$req->execute();
	}
}
