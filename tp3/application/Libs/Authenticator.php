<?php


namespace Mini\Libs;


use Mini\Model\Model;

class Authenticator {
	public function __construct() {
		if(session_status() == PHP_SESSION_NONE) {
			session_set_cookie_params(['path'=> '/tp3', 'httponly'=>true]);
			session_start();
		}
	}

	public function isLogged(){
		return isset($_SESSION['user']);
	}

	public function login($id, $pass){
		$m = new Model();
		if(!self::isLogged() && $m->verifyPassword($id, $pass)){
			$_SESSION['user'] = $id;
			return true;
		}
		return false;
	}

	public function logout(){
		if(self::isLogged()){
			unset($_SESSION['user']);
		}
	}

	public function checkLogin() {
		if(!$this->isLogged())
			Helper::redirect(URL.'index/login');
	}

	public function checkLoginOnLoginPages(){
		if($this->isLogged())
			Helper::redirect(URL);
	}

	public function getCurrentUserId(){
		return $_SESSION['user'];
	}
}