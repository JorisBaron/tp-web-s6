CREATE DATABASE IF NOT EXISTS `galilee_web_s6` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `galilee_web_s6`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id` varchar(50) NOT NULL,
    `nom` varchar(255) NOT NULL,
    `prenom` varchar(255) NOT NULL,
    `pass` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `visits`;
CREATE TABLE IF NOT EXISTS `visits` (
    `nb` int(11) NOT NULL,
    PRIMARY KEY (`nb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `visits` (`nb`) VALUES (0);
