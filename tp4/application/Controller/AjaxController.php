<?php


namespace Mini\Controller;


use Mini\Core\Renderer\JsonRenderer;

class AjaxController extends AbstractController {
	private const COUNTRIES_PATH = ROOT.'data/countries.json';

	public function getCountries(){
		$this->app->setRenderer(new JsonRenderer(true));
		return file_get_contents(self::COUNTRIES_PATH);
	}

	public function verifyCountry($name=null){
		$this->app->setRenderer(new JsonRenderer());

		$countries = json_decode(file_get_contents(self::COUNTRIES_PATH));
		$values = array_column($countries,'value');

		return in_array($name, $values);
	}
}