<?php


namespace Mini\Core\Renderer;


use Mini\Core\Application;

class JsonRenderer implements RendererInterface {
	private $alreadyJson;
	public function __construct($alreadyJson = false) {
		$this->alreadyJson = $alreadyJson;
	}

	public function render(Application $app) {
		header('Content-Type: application/json');
		$data = $app->viewData;
		if(!$this->alreadyJson)
			$data = json_encode($data);
		echo $data;
	}
}