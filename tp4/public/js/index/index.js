$(document).ready(function() {

	const alertTemplate = $(
		'<div class="alert alert-dismissible fade show" role="alert">' +
			'<span></span>' +
			'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
				'<span aria-hidden="true">&times;</span>' +
			'</button>' +
		'</div>');

	const select = $('#countrySelect');

	let countries = [];

	$.get(url + 'ajax/get-countries/',
		function(response) {
			if(Array.isArray(response)){
				countries = response;
				countries.forEach(function(row) {
					select.append(
						$('<option>').val(row.value).text(row.name));
				});
				select.prop('disabled',false);
			}
		})

	$('#countryForm').submit(function(e) {
		e.preventDefault();
		$('.alert').alert('close');

		const alert = alertTemplate.clone();
		const alertText = alert.children('span');

		let pays = select.val();
		const values = countries.map(function(val) {
			return val.value;
		});

		if(values.includes(pays)) {
			$.get(
				url + 'ajax/verify-country/' + pays,
				function(response) {
					if(response === true) {
						alert.addClass('alert-success');
						alertText.text("Sélection validée");
					} else {
						alert.addClass('alert-danger');
						alertText.text("Sélection invalide");
					}

					$('#countryForm').before(alert);
				}
			);
		} else {
			alert.addClass('alert-danger');
			alertText.text("Sélection invalide");
			$('#countryForm').before(alert);
		}
	});


})