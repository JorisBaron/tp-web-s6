<?php
	/*$table = array_map(function ($e){
		$m = $e['moyenne'] = round(array_sum($e['grades'])/count($e['grades']),2);
		$e['mention'] =
			($m<10) ? 'Redoublement' : (
				($m<12) ? 'Passage' : (
					($m<14) ? 'Mention assez bien' : (
						($m<16) ? 'Mention bien' :
							'Mention très bien')));
		return $e;
	}, json_decode(file_get_contents('eleves.json'),true));

	usort($table, function ($a, $b){
		return $a['moyenne']-$b['moyenne'];
	});*/

$elevesFile = file_get_contents('./eleves.json');
$eleves_data = json_decode($elevesFile);
$eleves = array();

function getMention($moyenne){
	if($moyenne<10){
		return "Redoublement";
	} else if($moyenne < 12){
		return "Passage";
	} else if($moyenne < 14){
		return "Mention assez bien";
	} else if($moyenne < 16){
		return "Mention bien";
	} else {
		return "Mention très bien";
	}
}

function comparer_moyennes($a,$b){
	return ($a['Moyenne'] - $b['Moyenne']);
}

for($i=0;$i<count($eleves_data);$i++){
	$moyenne = (float)array_sum($eleves_data[$i]->grades)/count($eleves_data[$i]->grades);
	//$moyenne = number_format($moyenne,2,',',' ');
	$eleves[$i] = [
		'NomComplet' => $eleves_data[$i]->firstName . strtoupper($eleves_data[$i]->name),
		'Moyenne' => $moyenne,
		'Message' => getMention($moyenne)
	];
}

usort($eleves, "comparer_moyennes");
var_dump($eleves);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Tableau</title>
		<style>
			table {
				border: 1px solid black;
				border-collapse: collapse
			}
			td, th{
				border: 1px solid black;
				padding: 5px;
			}
		</style>
	</head>
	<body>
		<table>
			<tr>
				<th>Nom</th>
				<th>Moyenne</th>
				<th>Commentaire</th>
			</tr>
			<?php foreach($eleves as $e) :?>
			<tr>
				<td><?=ucfirst($e['NomComplet'])?> <?= ''//strtoupper($e['name']) ?></td>
				<td><?=$e['Moyenne']?></td>
				<td><?=$e['Message']?></td>
			</tr>
			<?php endforeach;?>
		</table>
	</body>
</html>