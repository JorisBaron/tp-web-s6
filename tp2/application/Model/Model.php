<?php

namespace Mini\Model;

class Model
{
	private const SAVE_PATH = ROOT . 'data/';
	private const USERS_PATH = self::SAVE_PATH . 'users.json';
	private const COUNT_PATH = self::SAVE_PATH . 'count';

	public function __construct() {
		if(!file_exists(self::USERS_PATH) || file_get_contents(self::USERS_PATH) === ''){
			file_put_contents(self::USERS_PATH, "[]");
		}

		if(!file_exists(self::COUNT_PATH) || file_get_contents(self::COUNT_PATH) === ''){
			file_put_contents(self::COUNT_PATH, 0);
		}
	}

	/**
	 * @return object
	 */
	private function loadUsers() {
		return json_decode(file_get_contents(self::USERS_PATH));
	}

	private function saveUsers($users) {
		file_put_contents(self::USERS_PATH,json_encode($users));
	}

	public function addUser(string $id, string $nom, string $prenom, string $pass){
		$users = $this->loadUsers();
		if(!isset($users->$id)){
			$users->$id = [
				'nom' => $nom,
				'prenom' => $prenom,
				'pass' => password_hash($pass, PASSWORD_DEFAULT)
			];
		}
		$this->saveUsers($users);
	}

	/**
	 * @param string $id
	 * @return bool|object
	 */
	public function getUser(string $id)  {
		$users = $this->loadUsers();
		return $users->$id ?? false;
	}

	public function verifyPassword(string $id, string $pass)  {
		$user = $this->getUser($id);
		return $user!==false && password_verify($pass, $user->pass);
	}


	private function loadCount() : int {
		return json_decode(file_get_contents(self::COUNT_PATH));
	}

	private function saveCount(int $c) : void{
		file_put_contents(self::COUNT_PATH,json_encode($c));
	}

	public function getCount() : int{
		return $this->loadCount();
	}

	public function incrementCount() : void {
		$this->saveCount((int)$this->loadCount() + 1);
	}
}
